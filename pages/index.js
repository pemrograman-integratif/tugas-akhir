import {
  Box,
  Button,
  ButtonGroup,
  Table,
  TableContainer,
  Tbody,
  Tr,
  Th,
  Td,
  Thead,
  useColorModeValue,
  Link,
} from "@chakra-ui/react";
import { useRouter } from "next/router";

import { use, useContext, useEffect, useState } from "react";
import backend from "../api/backend";

export default function Home() {
  const [books, setBooks] = useState([]);
  const router = useRouter();

  const getAllBooks = async () => {
    try {
      const res = await backend.get("/getAll");

      setBooks(res.data.data);

    } catch (error) {
      console.log(error);
    }
  };


  useEffect(() => {
    getAllBooks();
  });

  return (
    <Box
      justifyContent="center"
      alignItems="center"
      minH="100vh"
      bg={useColorModeValue("gray.50", "gray.800")}
      pt={5}
      pb={10}
      px={10}
    >
      <Button
        pos="absolute" right="20"
        size="sm"
        colorScheme="green"
        onClick={() => router.push(`/create`)}
      >
        Add New Book
      </Button>
      <Box
        rounded="lg"
        bg={useColorModeValue("black.50", "gray.700")}
        p={8}
        pt={10}
        boxShadow="lg"
      >
    
        <TableContainer>
          <Table
                css={{
                  height: "auto",
                  minWidth: "100%",
                }}
          >
            <Thead
              bg={useColorModeValue("lightgrey")} 
            >
              <Tr>
                <Th>No</Th>
                <Th>Judul</Th>
                <Th>ISBN</Th>
                <Th>Penulis</Th>
                <Th>Penerbit</Th>
                <Th>Action</Th>
                <Th></Th>
              </Tr>
            </Thead>
            <Tbody>
              {books &&
                books.map((data, index) => (
                  <Tr key={data.isbn}>
                    <Td>{index + 1}</Td>
                    <Td>{data.judul}</Td>
                    <Td>{data.isbn}</Td>
                    <Td>{data.penulis}</Td>
                    <Td>{data.penerbit}</Td>
                    <Td>
                      <Button
                        size="sm"
                        colorScheme="blue"
                        onClick={() => router.push(`/update/${data.id}`)}
                      >
                        Update
                      </Button>
                    </Td>
                    <Td>
                      <Button
                        size="sm"
                        colorScheme="red"
                        onClick={() => router.push(`/delete/${data.id}`)}
                      >
                        Delete
                      </Button>
                    </Td>
                  </Tr>
                ))}
            </Tbody>
          </Table>
        </TableContainer>
      </Box>
     </Box>
  );
}
