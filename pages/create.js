import { useState } from "react";

import {
  Flex,
  Box,
  Stack,
  Text,
  Input,
  Button,
  Heading,
  FormControl,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Select,
  useColorModeValue,
  Link,
  Icon,
} from "@chakra-ui/react";

import { BiIdCard, BiLockAlt, BiShow, BiHide, BiUser } from "react-icons/bi";

import { useRouter } from "next/router";
import backend from "../api/backend";

const Register = () => {
  const [isbn, setISBN] = useState("");
  const [judul, setJudul] = useState("");
  const [penulis, setPenulis] = useState("");
  const [penerbit, setPenerbit] = useState("");

  const router = useRouter();


  const addBook = async (values) => {
    try {
      const res = await backend.post("/create", values, {
        validateStatus: false,
      });

      return res.data;
    } catch (error) {
      console.log(error);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const values = {
      isbn,
      judul,
      penulis,
      penerbit,
    };

    addBook(values);
    router.push("/");
  };

  return (
    <Flex
      justify="center"
      align="center"
      minH="100vh"
      bg={useColorModeValue("gray.50", "gray.800")}
    >
      <Stack spacing={8} mx="auto" minW="md" maxW="lg" py={12} px={6}>
        <Heading textAlign="center" fontSize="4xl">
          Add a New Book
        </Heading>

        <Box
          rounded="lg"
          bg={useColorModeValue("white", "gray.700")}
          p={8}
          boxShadow="lg"
        >
          <form onSubmit={handleSubmit}>
            <Stack spacing={4}>
              <FormControl>
                <InputGroup>
                  <InputLeftElement
                    children={
                      <Icon as={BiIdCard} w="6" h="6" color="gray.300" />
                    }
                  />
                  <Input
                    type="text"
                    placeholder="ISBN"
                    value={isbn}
                    onChange={(e) => setISBN(e.target.value)}
                  />
                </InputGroup>
              </FormControl>
              <FormControl>
                <InputGroup>
                  <InputLeftElement
                    children={<Icon as={BiUser} w="6" h="6" color="gray.300" />}
                  />
                  <Input
                    type="text"
                    placeholder="Judul"
                    value={judul}
                    onChange={(e) => setJudul(e.target.value)}
                  ></Input>
                </InputGroup>
              </FormControl>
              <FormControl>
                <InputGroup>
                  <InputLeftElement
                    children={<Icon as={BiUser} w="6" h="6" color="gray.300" />}
                  />
                  <Input
                    type="text"
                    placeholder="Penulis"
                    value={penulis}
                    onChange={(e) => setPenulis(e.target.value)}
                  ></Input>
                </InputGroup>
              </FormControl>
              <FormControl>
                <InputGroup>
                  <InputLeftElement
                    children={<Icon as={BiUser} w="6" h="6" color="gray.300" />}
                  />
                  <Input
                    type="text"
                    placeholder="Penerbit"
                    value={penerbit}
                    onChange={(e) => setPenerbit(e.target.value)}
                  ></Input>
                </InputGroup>
              </FormControl>
              <Stack spacing={10} pt={2}>
                <Button
                  type="submit"
                  value="submit"
                  size="lg"
                  bg={"blue.400"}
                  color={"white"}
                  _hover={{
                    bg: "blue.500",
                  }}
                >
                  Add
                </Button>
              </Stack>
              <Stack pt={6}>
                <Text align="center">
                  <Link color="blue.400" href="./">
                    Back
                  </Link>
                </Text>
              </Stack>
            </Stack>
          </form>
        </Box>
      </Stack>
    </Flex>
  );
};

export default Register;
