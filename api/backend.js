import axios from "axios";

export default axios.create({
  baseURL: "https://us-central1-bookshelf-3f329.cloudfunctions.net/app/api/",
});
